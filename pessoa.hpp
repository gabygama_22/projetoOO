#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <iostream>
#include <string>

using namespace std;

class Pessoa {
private:
//Atributos
        string nome;
        int idade;
        string telefone;
//Métodos
public:
       Pessoa();//Construtor
       ~Pessoa();//Destrutor:
       Pessoa(string nome, int idade, string telefone);
       int getIdade();
       void setIdade(int idade);
       string getNome();
       void setNome(string nome);
       string getTelefone();
       void setTelefone (string telefone);
       void incrementaIdade();
};
#endif

